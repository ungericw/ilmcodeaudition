﻿using System.Collections.Generic;
using Ilm.CodeAudition.Service;
using Ilm.CodeAudition.Service.Models;
using Microsoft.AspNetCore.Mvc;

namespace Ilm.CodeAudition.Web.Controllers
{
    public class TimesheetController : Controller
    {
        public ViewResult Index()
        {
            var service = new TimeTrackerService();
            return View(service.GetAll());
        }

        [HttpPost]
        public ViewResult Index(IEnumerable<Timesheet> timesheets)
        {
            // Initate TimeTrackerService once.
            var service = new TimeTrackerService();

            foreach (var timesheet in timesheets)
            {
                service.Save(timesheet);
            }

            return View(service.GetAll());
        }
    }
}